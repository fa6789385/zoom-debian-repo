#!/usr/bin/env bash

OUTPUT="public/index.html"

deb=$(find public/ -maxdepth 1 -mindepth 1 -name *deb)
path=$(basename "$deb")

echo "<h2>Unofficial Zoom Debian/Ubuntu repository</h2>" > $OUTPUT
echo "</br>" >> $OUTPUT
echo "<ul>" >> $OUTPUT
echo "<li> Repository last updated: $(date +%F) </li>" >> $OUTPUT
echo "<li> Latest Zoom version: $(cat version.txt) </li>" >> $OUTPUT
echo "<li> Latest Zoom deb package: <a href="$path">$path</a> </li>" >> $OUTPUT
echo "</ul>" >> $OUTPUT
echo "</br>" >> $OUTPUT
echo "For more information, see <a href="https://gitlab.com/axil/zoom-debian-repo">https://gitlab.com/axil/zoom-debian-repo</a>" >> $OUTPUT
