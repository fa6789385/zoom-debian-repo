#!/usr/bin/env bash

repo_version=$(wget -O - "https://gitlab.com/axil/zoom-debian-repo/-/jobs/artifacts/main/file/version.txt?job=build")
zoom_version=$(wget --spider https://zoom.us/client/latest/zoom_amd64.deb 2>&1 | grep Location | sed -e 's/.*prod\/\(.*\)\/.*/\1/')

if [ "$zoom_version" = "$repo_version" ]; then
    exit 1
fi
